// Exercício 2
const usuarios = [
  { nome: 'Diego', idade: 23, empresa: 'Rocketseat' },
  { nome: 'Gabriel', idade: 15, empresa: 'Rocketseat' },
  { nome: 'Lucas', idade: 30, empresa: 'Facebook' },
];

// 2.1
const idades = usuarios.map(usuario => usuario.idade);
console.log(idades);

// 2.2 // Não
// const retorno = usuarios.filter(usuario => usuario.empresa == 'Rocketseat' && usuario.idade > 180);

// 2.3

const google = usuarios.find(usuario => usuario.empresa == 'Google');


// Exercício 3 

// 3.1
const arr = [1, 2, 3, 4, 5];
arr.map(item => item + 10);

// 3.2

const usuario = { nome: 'Diego', idade: 23 };

const mostraIdade = ({ idade }) => { return idade };

mostraIdade(usuario);

// 3.3

const nome = "Diego";
const idade = 23;
const mostraUsuario = (nome = 'Diego', idade = 18) => { return { nome, idade } };
console.log(mostraUsuario());


// Exercício 4 
const empresa = {
	nome: 'Rocketseat',
	endereco: {
		cidade: 'Rio do Sul',
		estado: 'SC',
	}
};

// 4.1

const { nome, endereco: { cidade, estado } } = empresa;

// 4.2 // Não

// function mostraInfo(usuario) {
// 	return `${{ nome }} tem ${ {idade} } anos.`;
// }

// mostraInfo({ nome: 'Diego', idade: 23 });


// Exercício 5

// 5.1 => REST
const arr = [1, 2, 3, 4, 5, 6];
const [ x, ...y ] = arr;

function soma(...params) {
	return params.reduce((total, next) => total + next);
}

console.log(soma(2, 4, 5, 6));
console.log(soma(2, 2));

// 5.2 => SPREAD
const usuario = {
  nome: 'Diego',
  idade: 23,
  endereco: {
    cidade: 'Rio do Sul',
    uf: 'SC',
    pais: 'Brasil',
  }
};

const usuario2 = { ...usuario, nome: 'Gabriel' };

// const usuario3 = ???

// Exercício 6

const usuario = 'Diego';
const idade = 23;

console.log(`O usuário ${usuario} possui ${idade} anos`);

// Exercício 7

const nome = 'Diego';
const idade = 23;

const usuario = {
  nome,
  idade,
  cidade: 'Rio do Sul',
}  
// var xhr = new XMLHttpRequest();

// xhr.open('GET', 'https://api.github.com/users/willian-fer')
// xhr.send(null);

// xhr.onreadystatechange = function() {
// 	if (xhr.readyState === 4) {
// 		console.log(JSON.parse(xhr.responseText));
// 	}
// }


// Promises
// Não influencia da linha do tempo do código, 

// var minhaPromise = function() {
// 	// resolve = utilizamos quando resultado foi de sucesso, reject é o contrario
// 	return new Promise(function(resolve, reject) {
// 		var xhr = new XMLHttpRequest();
// 		// xhr.open('GET', 'https://api.github.com/users/willian-fer');
// 		xhr.open('GET', 'https://api.github.com/users/willian-dddfer'); // Com erro
// 		xhr.send(null);
// 		xhr.onreadystatechange = function() {
// 			if (xhr.readyState === 4) {
// 				if (xhr.status === 200) {
// 					resolve(JSON.parse(xhr.responseText));
// 				} else {
// 					reject('Erro na requisição')
// 				}
// 			}
// 		}
// 	});	
// }

// var resultado = minhaPromise()
// console.log(resultado);
// minhaPromise()
// 	.then(function(response){
// 		console.log(response);
// 	})
// 	.catch(function(error){
// 		console.warn(error);
// 	});

// Ultilizando Axios

axios.get('https://api.github.com/users/willian-fer')
	.then(function(response){
		console.log(response);
	})
	.catch(function(error){
		console.warn(error);
	});

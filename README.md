Aplicado no front-end da maioria das aplicações web hoje em dia e com nodejs pode ser usado no back-end. Com react pode ser feito app mobile.

- Variáveis
- Funcões 
- Condicionais
- Estruturas de repetição
- Manipulação de DOM
- Requisições assíncronas
- Construindo um app do Zero

``` javascript
var nome = "Willian"; //string
var idade = 18; //int
var peso = 60.2; //decimais
var humano = true; //boolean

var alunos = ['Willian', 'Ruan', 'Victor'] // array | vetor

var aluno = {
    nome: 'Willian',
    idade: 18,
    peso: 60.2,
    humano: true
}

console.log(nomeVar);
console.log(vetor[1]); //posição = []
console.log(objeto.item);
```

&& = and
|| = or
!= = not

``` javascript
// ternário
 var sexo = 'M';

 return (sexo === 'M') ? 'Masculino' : 'Feminino';
```

``` javascript
for (var i = 0; i <= 100; i++) {
    console.log(i);
}

var j = 0;
while (j <= 100) {
    console.log(j);
}

```

### DOM

- Dom: é a árvore de elementos do navegador

.document => é uma variável que referência a DOM do arquivo.


https://github.com/RocketSeat/curso-javascript-do-zero/blob/master/tarefas/modulo01.md

https://github.com/RocketSeat/curso-javascript-do-zero/blob/master/tarefas/modulo02.md

https://github.com/RocketSeat/curso-javascript-do-zero/blob/master/tarefas/modulo04.md


    // -w monitora os arquivos sempre que ouver alterações

Método estatico serve para retornar dados independete da classe
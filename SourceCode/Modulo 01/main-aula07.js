const arr = [2, 3, 4, 7, 9];

// Forma verbosa
const newArr = arr.map(function(item, index) {
    return item + index;
});

const newArray = arr.map((item, index) => item + index);

const sum = arr.reduce((total, next) => total + next);

const filter = arr.filter(item => item % 2 === 0);

const find = arr.find(item => item === 4);

const teste = () => {
    console.log('Arrow Function')
}

const teste2 = () => [2, 3, 4];
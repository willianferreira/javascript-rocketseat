const nome = 'Willian';
const idade = '18';

// Quando a propriedade do objeto é igual a variavel, nao precisa atribuir
const usuario = {
    nome,
    idade,
    empresa: 'Nenhuma'
};

console.log(usuario);
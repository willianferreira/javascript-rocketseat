// Operadores rest e spread

/**
* REST
*/

const usuario = {
    nome: 'Willian',
    idade: 18,
    empresa: 'Nenhuma' 
};

const { nome, idade, ...resto } = usuario;

console.log(nome);
console.log(resto);

const arr = [1, 3, 5, 6, 8];

const [ a, b, ...c ] = arr;

console.log(a);
console.log(b);
console.log(c);

function soma(a, b, ...params) {
    console.log(params)
    return params.reduce((total, next) => total + next); 
}
console.log(soma(1, 4, 3, 4, 3));

// SPREAD => Propagar (repassar informaçõe obj ou array para outra propriedade)

const arr1 = [1, 3, 4];
const arr2 = [7, 9, 10];

const arr3 = [ ...arr1, ...arr2 ];
console.log(arr3);

const usuario1 = {
    nome: 'Willian',
    idade: 18,
    empresa: 'Nenhuma'
};

// Pega todas as propriedas do usuario1 e sobrescreve o nome
const usuario2 = { ...usuario1, nome: 'Outro nome' };
console.log(usuario2);

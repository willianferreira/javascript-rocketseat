// Aula 04 => Classes 

class Matematica {
    /*
     *  Se a classe é estatica ela não enxerga o resto da classe
     *  Variáveis globais ou outras funcões. Não precisa de instância
     */ 
    static soma(a, b) {
        return a + b;
    }
}


class List {

    constructor() {
        this.data = [];
    }

    add(data) {
        this.data.push(data);
        console.log(this.data);
    }
}

class TodoList extends List {

    // constructor() {
    //     this.todos = [];
    // }

    // addTodo() {
    //     this.todos.push('Novo Todo');
    //     console.log(this.todos);
    // }

}

var minhaLista = new TodoList();

document.getElementById('novotodo').onclick = function() {
    // minhaLista.addTodo();
    minhaLista.add('Teste');
}

console.log(Matematica.soma(2, 2));
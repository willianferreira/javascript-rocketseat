const usuario = {
    nome: 'Willian',
    idade: 23,
    endereco: {
        cidade: 'São Leopoldo',
        estado: 'RS',
    }, 
};

// const nome = usuario.nome;
// const idade = usuario.idade;
// const cidade = usuario.endereco;

/**
* Desestruturação do objeto
*/

const { nome, idade, endereco: { cidade } } = usuario;

console.log(nome);
console.log(idade);
console.log(cidade);


/**
* Desestruturação de objeto funciona como parâmetro também
*/
function mostraDados({ nome, idade }) {
    console.log(nome, idade);
}

mostraDados(usuario);
 